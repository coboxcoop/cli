const os = require('os')

module.exports = {
  address: {
    description: 'address of the folder',
    type: 'string',
    alias: 'a',
  },
  name: {
    description: 'name of the folder',
    type: 'string',
    alias: 'n'
  },
  key: {
    description: 'encryption key of the folder',
    type: 'string',
    alias: 'e',
  },
  friendsKey: {
    description: 'key of a friend',
    type: 'string',
    alias: 'k',
  },
  code: {
    description: 'an invite code',
    type: 'string',
    alias: 'c',
  },
  seeder: {
    description: 'name of the registered seeder',
    type: 'string',
    alias: 'd'
  },
  dir: {
    description: 'specify a path',
    type: 'string'
  },
  port: {
    alias: 'p',
    number: true,
    describe: 'server port',
    default: 9112
  },
  timeout: {
    alias: 't',
    number: true,
    describe: 'set a timeout',
    default: 3000
  },
  file: {
    alias: 'f',
    type: 'string',
    describe: 'e.g., /path/to/backup.cobox'
  },
  location: {
    type: 'string',
    describe: 'e.g. /path/to/backup.cobox',
    default: os.homedir()
  },
  raw: {
    type: 'boolean',
    describe: 'output raw data to stdout',
    default: false
  },
  password: {
    type: 'string',
    describe: 'your decryption password'
  }
}
