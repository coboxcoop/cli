exports.command = 'folders <command>'
exports.desc = 'view and interact with your folders'
exports.builder = function (yargs) {
  return yargs
    .commandDir('folders')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
