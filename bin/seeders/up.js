const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')
const options = require('../lib/options')

exports.command = 'up [code]'
exports.desc = 'open connection with a seeder or all registered seeders'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['code']))
exports.handler = up

async function up (argv) {
  const opts = argv
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const code = opts.code

  try {
    if (code) {
      let seeder = await client.post(['admin', 'seeders'], {}, { code })
      return await onceSeeder(seeder)
    } else {
      let seeders = await client.get(['admin', 'seeders'])

      if (!seeders.length) {
        return console.log(`! ${chalk.bold(`you have no registered seeders`)}, join one with\n${chalk.hex('#ACFFAC')(`seeders up --code <code>`)}`)
      }

      seeders = await client.post(['admin', 'seeders', 'connections'])
      seeders.forEach(printSeeder)
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSeeder (seeder) {
    seeder = await client.post(['admin', 'seeders', seeder.address, 'connections'], {}, seeder)
    print(seeder)
  }
}

function printSeeder (seeder) {
  console.log()
  print(seeder)
}
