const ISSUE_URL = 'http://gitlab.com/coboxcoop/seeder/issues'

const UNAUTHORIZED = (err) => {
  return `not authorized, please log in to cobox using \'cobox login <password>\'`
}

const ECONNABORTED = (err) => {
  return `cobox is running but failed to respond.

try restarting with \'cobox stop\', then \'cobox start\'.

if that fails, please open an issue at ${ISSUE_URL} and provide the following error stack with steps to reproduce:

${JSON.stringify(err.toJSON(), null, 2)}`
}

const ECONNREFUSED = (err) => {
  return 'cobox is not running on this port, either specify the correct port or start the app using \'cobox start\''
}

const UNHANDLED_CLI_ERROR = (err) => {
  console.log(`there has been an unexpected command line error... please open an issue at ${ISSUE_URL} and provide the following error stack with steps to reproduce`)
  console.log()
  console.error(err)
}

const UNHANDLED_SERVER_ERROR = (err) => {
  return `there has been an unexpected server error... please email bugs@cobox.cloud with main.log

${JSON.stringify(err.toJSON(), null, 2)}`
}

module.exports = {
  UNAUTHORIZED,
  ECONNABORTED,
  ECONNREFUSED,
  UNHANDLED_CLI_ERROR,
  UNHANDLED_SERVER_ERROR
}
