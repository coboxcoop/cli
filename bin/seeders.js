exports.command = "seeders <command>"
exports.desc = "send commands to a remote seeder"
exports.builder = function (yargs) {
  return yargs
    .commandDir('seeders')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
