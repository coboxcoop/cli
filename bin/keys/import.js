// const Client = require('@coboxcoop/client')
// const fs = require('fs')
// const pick = require('lodash.pick')
// const { buildBaseURL } = require('@coboxcoop/client/util')
// const chalk = require('chalk')
// const FormData = require('form-data')
// const options = require('../lib/options')
// const path = require('path')

// exports.command = 'import <file>'
// exports.desc = 'import a backup.cobox file'
// exports.builder = Object.assign({}, constants.serverDefaults, pick(options, ['file']))
// exports.handler = async function (argv) {
//   const client = new Client({ endpoint: buildBaseURL(argv) })
//   const file = path.resolve(process.cwd(), argv.file)

//   try {
//     const form = new FormData()
//     form.append('export', fs.createReadStream(file))
//     let data = await client.post('import', null, form, {
//       headers: form.getHeaders()
//     })
//     console.log("imported successfully, rebuilding from import...")
//   } catch (err) {
//     if (!err.response) return console.trace(err)
//     var errors = err.response.data.errors
//     if (!Array.isArray(errors)) return console.error('there has been an unexpected error... please email bugs@cobox.cloud with main.log')
//     errors.forEach((err) => process.stderr.write(err.msg))
//   }
// }
