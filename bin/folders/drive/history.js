const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../../lib/util')

exports.command = 'history [options]'
exports.desc = 'get drive history for a given folder'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name', 'address']))
exports.handler = history

async function history (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let history = await client.get(['spaces', name || address, 'drive', 'history'])
    print(history)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
