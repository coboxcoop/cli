exports.command = 'seeds <command>'
exports.desc = 'ask a seeder to seed a folder'
exports.builder = function (yargs) {
  return yargs
    .commandDir('seeds')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
