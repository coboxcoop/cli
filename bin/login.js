const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const { getDefaultOpts, handleAjaxErrors } = require('./lib/util')

exports.command = 'login <password>'
exports.desc = 'login (or register) to unlock your cobox spaces'
exports.builder = getDefaultOpts()
exports.handler = login

async function login (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  try {
    await client.post('auth', {}, { password: argv.password })
    console.log('logged in to cobox')
  } catch (err) {
    if (err.response && err.response.status === 404) {
      try {
        await client.post('register', {}, { password: argv.password })
        console.log('registered and logged in to cobox')
      } catch (err) {
        handleAjaxErrors(err)
      }
    } else {
      handleAjaxErrors(err)
    }
  }
}
