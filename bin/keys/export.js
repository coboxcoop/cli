const Client = require('@coboxcoop/client')
const pick = require('lodash.pick')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const concat = require('concat-stream')
const tar = require('tar-stream')
const zlib = require('zlib')
const pumpify = require('pumpify')
const options = require('../lib/options')
const fs = require('fs')
const path = require('path')
const { DEFAULT_COLOUR, getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'export <password> [options]'
exports.desc = 'export your secret keys and configuration file'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['location', 'raw', 'json']))
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let data = await client.get('export', { password: argv.password }, {
      headers: { 'Content-Type': 'application/octet-stream' },
      responseType: 'stream'
    })
    if (argv.raw) {
      return data.pipe(process.stdout)
    } else if (argv.json) {
      const files = {}
      const extract = tar.extract()
      extract.on('entry', (header, stream, next) => {
        stream.pipe(concat((contents) => {
          files[header.name] = JSON.parse(contents.toString('utf-8'))
          return next()
        }))
      })
      extract.on('finish', () => console.log(JSON.stringify(files, null, 2)))
      pumpify(data, zlib.createUnzip(), extract)
    } else {
      var location = path.join(argv.location, 'cobox.backup')
      data.pipe(fs.createWriteStream(location))
      console.log(`export written to ${chalk.hex(DEFAULT_COLOUR)(location)}`)
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
