const constants = require('@coboxcoop/constants')
const options = require('./options')
const DEFAULT_COLOUR = '#ACFFAC'
const chalk = require('chalk')

const {
  UNAUTHORIZED,
  ECONNABORTED,
  ECONNREFUSED,
  UNHANDLED_CLI_ERROR,
  UNHANDLED_SERVER_ERROR
} = require('./error-messages')

function getDefaultOpts () {
  return Object
    .keys(constants.serverDefaults)
    .filter((k) => options[k])
    .reduce((acc, key) => { acc[key] = options[key]; return acc }, {})
}

function handleAjaxErrors (err) {
  if (err.response) {
    // the request was made and the server responded with a status code that is outside of the range 2xx
    var errors = err.response.data.errors
    // something wierd occurred server side
    if (err.response.status === 401) return console.log(UNAUTHORIZED(err))
    if (err.response.status === 400) return console.log(UNHANDLED_SERVER_ERROR(err))
    if (err.response.status === 500) return console.log(UNHANDLED_SERVER_ERROR(err))
    if (!Array.isArray(errors)) return console.log(UNHANDLED_SERVER_ERROR(err))
    errors.forEach((err) => {
      if (err.param) {
        let msg = `${chalk.hex('#ACFFAC')(err.param)} ${err.msg}`
        console.log(msg)
      } else {
        console.log(err.msg)
      }
    })
  } else if (err.request) {
    // the request was made but no response received, could have been a timeout, or the wrong port was specified
    if (err.code === 'ECONNREFUSED') return console.log(ECONNREFUSED(err))
    if (err.code === 'ECONNABORTED') return console.log(ECONNABORTED(err))
    // TODO: not correctly handling other errors at the moment
    return console.log(ECONNREFUSED(err))
  } else {
    // something happened when generating the request that caused an error
    return UNHANDLED_CLI_ERROR(err)
  }
}

module.exports = {
  DEFAULT_COLOUR,
  getDefaultOpts,
  handleAjaxErrors
}
