const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const path = require('path')
const options = require('../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'invite <name> <friends-key>'
exports.desc = 'invite a friend to a folder'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name', 'friendsKey']))
exports.handler = invite

async function invite (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const publicKey = argv.friendsKey
  const name = argv.name

  try {
    var spaces = await client.get('spaces')
    var space = spaces.find((space) => space.name === name)
    if (space) return await onceSpace()
    else return console.log(`! ${chalk.bold(`folder doesn't exist`)}`)
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSpace () {
    let data = await client.post(['spaces', space.address, 'invites'], {}, { publicKey })
    if (data.response) return console.error(data.response.data.errors)
    print({ code: data.content.code })
  }
}

