const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const options = require('../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'ls'
exports.desc = 'view your folders'
exports.builder = getDefaultOpts()
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let spaces = await client.get('spaces')
    if (!spaces.length) return console.log(`! ${chalk.bold(`you have no folders`)} create one with\n${chalk.hex('#ACFFAC')(`cobox folders up --name <name>`)}`)
    spaces.forEach(printSpace)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}

function printSpace (space) {
  console.log()
  print(space)
}
