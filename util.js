const chalk = require('chalk')

function readableBytes (bytes) {
  if (bytes < 1) return 0 + ' B'
  const i = Math.floor(Math.log(bytes) / Math.log(1024))
  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i]
}

function printBanner () {
console.log(`
                [0;34m888[0m                      
                [0;34m888[0m                      
                [0;37m888[0m                      
 [0;34m.d8888b[0m [0;37m.d88b.[0m [0;37m88888b.[0m  [0;1;30;90m.d88b.[0m [0;1;30;90m888[0m  [0;1;30;90m888[0m 
[0;37md88P"[0m   [0;37md88""88b[0;1;30;90m888[0m [0;1;30;90m"88bd88""88b[0;1;34;94m\`Y8bd8P'[0m 
[0;37m888[0m     [0;1;30;90m888[0m  [0;1;30;90m888888[0m  [0;1;30;90m888[0;1;34;94m888[0m  [0;1;34;94m888[0m  [0;1;34;94mX88K[0m   
[0;1;30;90mY88b.[0m   [0;1;30;90mY88..88P[0;1;34;94m888[0m [0;1;34;94md88PY88..88P[0;34m.d8""8b.[0m 
 [0;1;30;90m"Y8888P[0m [0;1;34;94m"Y88P"[0m [0;1;34;94m88888P"[0m  [0;34m"Y88P"[0m [0;34m888[0m  [0;34m888[0m 
`)
}

function buildBaseURL (opts) {
  return ['http://', opts.hostname || 'localhost', ':', opts.port || 9112, '/api'].join('')
}

module.exports = {
  readableBytes,
  printBanner,
  buildBaseURL
}
