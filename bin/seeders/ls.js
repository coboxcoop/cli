const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'ls'
exports.desc = 'view your seeders'
exports.builder = getDefaultOpts()
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let seeders = await client.get(['admin', 'seeders'])
    if (!seeders.length) return console.log(`! ${chalk.bold(`you have no seeders`)} you need an invite code from a seeder\n`)
    seeders.forEach(printSeeder)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}

function printSeeder (seeder) {
  print(seeder)
  console.log()
}
