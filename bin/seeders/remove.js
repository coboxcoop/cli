const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const pick = require('lodash.pick')
const options = require('../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'remove <name>'
exports.desc = 'remove a seeder permanently from disk'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name']))
exports.handler = down

async function down (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    let seeders = await client.get(['admin', 'seeders'])
    let seeder = seeders.find((seeder) => seeder.name === name)
    if (seeder) return await onceSeeder(seeder)
    else return console.log(`! ${chalk.bold(`seeder doesn't exist`)}`)
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSeeder (seeder) {
    await client.delete(['admin', 'seeders', seeder.address], {}, seeder)
    console.log(`${chalk.bold(`Seeder removed successfully.`)}\n`)
    print(seeder)
  }
}
