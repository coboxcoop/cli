#!/usr/bin/env node
const fs = require('fs')
const yargs = require('yargs')
const path = require('path')
const os = require('os')
const constants = require('@coboxcoop/constants')
const YAML = require('js-yaml')
const config = require('@coboxcoop/config')

const args = yargs
  .option('config', { describe: 'path to .coboxrc file', default: path.join(os.homedir(), '.coboxrc') })
  .middleware((argv) => Object.assign(argv, config.load(argv.config)))
  .commandDir('bin')
  .demandCommand()
  .alias('h', 'help')
  .alias('v', 'version')
  .help()

if (require.main === module) return args
    .usage('Usage: $0 <command> [options]')
    .argv

module.exports = args
