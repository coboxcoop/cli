const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'down [name]'
exports.desc = 'close a folder or all folders'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name']))
exports.handler = down

async function down (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    let spaces = await client.get('spaces')

    if (!name) {
      let responses = await Promise.all([
        client.delete(['spaces', 'connections']),
        client.delete(['spaces', 'mounts'])
      ])
      spaces = responses[0]
      spaces.forEach(printSpace)
    } else {
      let space = spaces.find((space) => space.name === name)
      if (space) return await onceSpace(space)
      else return console.log(`! ${chalk.bold(`folder doesn't exist`)}`)
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSpace (space) {
    let responses = await Promise.all([
      client.delete(['spaces', space.address, 'connections'], {}, space),
      client.delete(['spaces', space.address, 'mounts'], {}, space)
    ])
    space = responses[0]
    printSpace(space)
  }
}

function printSpace (space) {
  console.log()
  print(space)
}
