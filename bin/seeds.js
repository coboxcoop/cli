exports.command = 'seeds <command>'
exports.desc = "seed your friend's folder on your local device"
exports.builder = function (yargs) {
  return yargs.commandDir('seeds')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
