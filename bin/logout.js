const os = require('os')
const fs = require('fs')
const path = require('path')
const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('./lib/util')

exports.command = 'logout'
exports.desc = 'lock your cobox spaces'
exports.builder = getDefaultOpts()
exports.handler = login

async function login (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    await client.delete('auth')
    await fs.promises.unlink(path.join(os.tmpdir(), 'cobox', 'sid'))
    console.log('logged out of cobox')
  } catch (err) {
    print('failed to logout')
    return handleAjaxErrors(err)
  }
}
