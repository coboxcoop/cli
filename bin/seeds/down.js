const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')
const options = require('../lib/options')

exports.command = 'down [name]'
exports.desc = 'close a seed or all seeds'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name']))
exports.handler = down

async function down (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    if (!name) {
      var replicators = await client.delete('replicators/connections')
      replicators.forEach(printReplicator)
    } else {
      let replicators = await client.get('replicators')
      let replicator = replicators.find((replicator) => replicator.name === name)
      if (replicator) return await onceReplicator(replicator)
      else return console.log(`! ${chalk.bold(`seed doesn't exist`)}`)
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceReplicator (replicator) {
    await client.delete(`replicators/${replicator.address}/connections`, {
      id: replicator.address
    }, replicator)
    print(replicator)
  }
}

function printReplicator (replicator) {
  console.log()
  print(replicator)
}
