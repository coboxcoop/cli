const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../../lib/util')

exports.command = 'ls [options]'
exports.desc = 'list files in a drive directory'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name', 'address', 'dir']))
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address,
    dir = argv.dir

  try {
    let files = await client.get(['spaces', name || address, 'drive', 'readdir'], {}, { dir })
    files.forEach((file) => print(file))
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
