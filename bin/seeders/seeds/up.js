const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../../lib/util')

exports.command = 'up <seeder> [options]'
exports.desc = 'ask a seeder to seed a folder'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name', 'address', 'seeder']))
exports.handler = start

async function start (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const seeder = argv.seeder
  const name = argv.name
  const address = argv.address

  try {
    if (!name && !address) return console.log(`! ${chalk.bold(`you must provide a name and address`)}`)
    let d = await client.get(['admin', 'seeders', seeder])
    let command = await client.post(['admin', 'seeders', d.address, 'commands', 'replicate'], {}, { name, address })
    print(command.content)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
