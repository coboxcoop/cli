const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('./lib/util')

exports.command = 'whoami'
exports.desc = 'display your public name and cobox key'
exports.builder = getDefaultOpts()
exports.handler = whoami

async function whoami (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    var data = await client.get('/identities/current')
    print(data)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
