const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'remove <name>'
exports.desc = 'remove a seed permanently from disk'
exports.builder = getDefaultOpts()
exports.handler = remove

async function remove (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    let replicators = await client.get('replicators')
    let replicator = replicators.find((replicator) => replicator.name === name)
    if (replicator) return await onceSpace(replicator)
    else return console.log(`! ${chalk.bold(`Seed doesn't exist`)}`)
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSpace (replicator) {
    await Promise.all([
      client.delete(['replicators', replicator.address], {}, replicator)
    ])
    console.log(`${chalk.bold(`Seed removed successfully.`)}\n`)
    print(replicator)
  }
}
