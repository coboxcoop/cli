const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const print = require('pretty-print')
const pick = require('lodash.pick')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')
const options = require('../lib/options')

exports.command = 'remove <name>'
exports.desc = 'remove a space permanently from disk'
exports.builder = Object.assign({}, getDefaultOpts())
exports.handler = remove

async function remove (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    let spaces = await client.get('spaces')
    let space = spaces.find((space) => space.name === name)
    if (space) return await onceSpace(space)
    else return console.log(`! ${chalk.bold(`folder doesn't exist`)}`)
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSpace (space) {
    await client.delete(['spaces', space.address], {}, space)
    console.log(`${chalk.bold(`Folder removed successfully.`)}\n`)
    print(space)
  }
}
