const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')
const print = require('pretty-print')

exports.command = 'ls'
exports.desc = 'list your seeds'
exports.builder = getDefaultOpts()
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let replicators = await client.get('replicators')
    if (!replicators.length) return console.log(`! ${chalk.bold(`you are not seeding any addresses`)}\nget an address to start backing up someone's folder:\n${chalk.hex('#ACFFAC')(`cobox seeds up --address <address> --name <name>`)}`)
    replicators.forEach(printReplicator)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}

function printReplicator (replicator) {
  print(replicator)
  console.log()
}
