const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('./lib/util')

exports.command = 'nickname <name>'
exports.desc = 'change your public name'
exports.builder = getDefaultOpts()
exports.handler = nickname

async function nickname (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    const id = await client.get('identities/current')
    const data = await client.patch(`identities/${id.publicKey}`, {}, { name })
    print(data)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
