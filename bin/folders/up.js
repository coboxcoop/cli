const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'up [name|code]'
exports.desc = 'open a folder or all folders'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name', 'code']))
exports.handler = up

async function up (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name
  const code = argv.code

  try {
    let spaces = await client.get('spaces')

    if (code) {
      let space = await client.post('spaces', {}, { code })
      await onceSpace(space)
    } else if (!name) {
      if (!spaces.length) return console.log(`! ${chalk.bold(`you have no folders`)} create one with\n${chalk.hex('#ACFFAC')(`folders up --name <name>`)}`)
      let responses = await Promise.all([
        client.post(['spaces', 'connections']),
        client.post(['spaces', 'mounts'])
      ])
      spaces = responses[0]
      spaces.forEach(printSpace)
    } else {
      let space = spaces.find((space) => space.name === name)
      if (space) return await onceSpace(space)
      space = await client.post('spaces', {}, { name })
      await onceSpace(space)
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSpace (space) {
    let responses = await Promise.all([
      client.post(['spaces', space.address, 'mounts'], {}, space),
      client.post(['spaces', space.address, 'connections'], {}, space)
    ])
    space = responses[0]
    printSpace(space)
  }
}

function printSpace (space) {
  console.log()
  print(space)
}
