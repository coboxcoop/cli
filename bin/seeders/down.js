const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'down [name]'
exports.desc = 'close connection with a seeder or all registered seeders'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['name']))
exports.handler = down

async function down (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name

  try {
    let seeders = await client.get(['admin', 'seeders'])

    if (!name) {
      seeders = await client.delete(['admin', 'seeders', 'connections'])
      seeders.forEach(printSeeder)
    } else {
      let seeder = seeders.find((seeder) => seeder.name === name)
      if (seeder) return await onceSeeder(seeder)
      else return console.log(`! ${chalk.bold(`seeder doesn't exist`)}`)
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSeeder (seeder) {
    seeder = await client.delete(['admin', 'seeders', seeder.address, 'connections'], {}, seeder)
    print(seeder)
  }
}

function printSeeder (seeder) {
  console.log()
  print(seeder)
}
