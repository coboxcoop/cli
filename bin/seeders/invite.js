const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')
const print = require('pretty-print')
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')

exports.command = 'invite <name> <friends-key>'
exports.desc = 'invite a friend to be an admin for a seeder'
exports.builder = Object.assign({}, getDefaultOpts(), pick(options, ['friendsKey', 'address']))
exports.handler = invite

async function invite (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const publicKey = argv.friendsKey
  const name = argv.name

  try {
    var seeders = await client.get(['admin', 'seeders'])
    var seeder = seeders.find((seeder) => seeder.name === name)
    if (seeder) return await onceSpace()
    else return console.log(`! ${chalk.bold(`seeder doesn't exist`)}`)
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSpace () {
    let data = await client.post(['admin', 'seeders', seeder.address, 'invites'], {}, { publicKey })
    print({ code: data.content.code })
  }
}
