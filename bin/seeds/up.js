const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const print = require('pretty-print')
const { assign } = Object
const { getDefaultOpts, handleAjaxErrors } = require('../lib/util')
const options = require('../lib/options')

exports.command = 'up [options]'
exports.desc = 'open a seed or all seeds'
exports.builder = assign({}, getDefaultOpts(), pick(options, ['name', 'address']))
exports.handler = up

async function up (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name
  const address = argv.address

  try {
    let replicators = await client.get('replicators')

    if (!(name || address)) {
      if (!replicators.length) return console.log(`! ${chalk.bold(`you have no seeders`)} create one with\n${chalk.hex('#ACFFAC')(`cobox seeders up --name <name> --address <address>`)}`)
      replicators = await client.post(['replicators', 'connections'])
      replicators.forEach(printReplicator)
    } else {
      let replicator = replicators.find((replicator) => replicator.name === name || replicator.address === address)
      if (replicator) return await onceReplicator(replicator)
      if (!address) return console.log(`! ${chalk.bold(`you must provide an address`)}`)
      replicator = await client.post('replicators', {}, { name, address })
      await onceReplicator(replicator)
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceReplicator (replicator) {
    replicator = await client.post(['replicators', replicator.address, 'connections'], { id: replicator.address }, replicator)
    print(replicator)
  }
}

function printReplicator (replicator) {
  console.log()
  print(replicator)
}
